package com.example.tp2

import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import org.json.JSONObject
import java.io.*
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL

class MainActivity : AppCompatActivity() {
    var result: Boolean;
    init { result = false; }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.authentication);

        val login = findViewById<EditText>(R.id.auth_user);
        val pass = findViewById<EditText>(R.id.auth_pass);
        val submit = findViewById<Button>(R.id.auth_login);
        val result = findViewById<TextView>(R.id.auth_status);

        submit.setOnClickListener { view ->
            val thread = Thread {
                var url: URL? = null
                try {
                    url = URL("https://httpbin.org/basic-auth/bob/sympa")
                    val urlConnection: HttpURLConnection = url.openConnection() as HttpURLConnection;
                    urlConnection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((login.text.toString() + ":" + pass.text.toString()).toByteArray(), Base64.NO_WRAP));
                    try {
                        val instream: InputStream = BufferedInputStream(urlConnection.getInputStream())
                        val s: String = readStream(instream)
                        val res: Boolean = JSONObject(s).getBoolean("authenticated");
                        Log.i("JFL", s);

                        this.result = res;
                    } catch (e: IOException) {
                        this.result = false;
                    } finally {
                        urlConnection.disconnect();
                        runOnUiThread(Runnable {
                            result.setText(this.result.toString());
                        });
                    }
                } catch (e: MalformedURLException) {
                    e.printStackTrace();
                } catch (e: IOException) {
                    e.printStackTrace();
                }
            };

            thread.start();
        }

    }

    private fun readStream(instream: InputStream): String {
        val r = BufferedReader(instream.reader());
        val res = r.readText();
        r.close();
        instream.close();
        return res;
    }
}